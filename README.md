# Starfleet CI Templates
Templates for GitLab CI that can be referenced by other repositories.

## How to use
In the `gitlab-ci.yml` of the project that wants to use one of this templates one should have:
```yml
include:
  - project: starfleetsns/ci-templates
    ref: master
    file: TEMPLATE_FILE.yml
```
After this one generally needs to create a job on the pipeline that extends the included hidden job (which is named after the template file name) and customize it with needed variables, if any.
```yml
build_step_1:
  extends: .TEMPLATE_FILE
  variables:
    VAR1: OK
```

Please refer to [this blog post](https://dev.to/anhtm/a-comprehensive-guide-to-creating-your-own-gitlab-ci-template-library-5b3i) for more informations.

## Available templates
To configure the templates one can directly look inside their files.

### Docker image builder
Builds and pushes to a registry a docker image built using the Dockerfile in the referencing repository.
`docker_image_builder.yml` also supports multiple docker files in the same repository with different extensions.
If you want to build a dockerfile at a time, use `docker_image_builder_single.yml` with the configured variable `DOCKERFILE`, referring to the path of the dockerfile relative to the repository root.

## LaTeX compiler
Uses latexmk to compile your tex files inside of a repository and include the produced pdfs in the artifacts.
The variable `BUILD_PATTERN` can be used to build only tex files containing a certain string.
